export function ngRepeat() {
  const link = (scope, el, atr) => {
    const { parentNode, attributes, innerText } = el;
    const parentHtml = parentNode.innerHTML;
    const addElemToNode = () => {
      parentNode.innerHTML = parentHtml;
      const element = parentNode.querySelector('[ng-repeat]');
      const atrVal = attributes['ng-repeat'].value.split(' ');
      const nodeForElements = document.createDocumentFragment();
      const repeatInitVal = scope[atrVal[2]];
      element.innerText = repeatInitVal[0] ? repeatInitVal[0] : element.style.display = 'none';

      for (let i = 1; i < repeatInitVal.length; i++) {
        const newEl = element.cloneNode();
        newEl.innerText = innerText.replace(/{{.+}}/, repeatInitVal[i]);
        nodeForElements.append(newEl);
      }

      parentNode.appendChild(nodeForElements);
    };
    addElemToNode();

    scope.$watch('ngRepeat', addElemToNode);
  };

  return {
    link
  };
}

export function ngInit() {
  function link(scope, el, atr) {
    const value = el.getAttribute('ng-init');
    eval.call(window, value);
  }

  return {
    link
  };
}
export function ngModel() {
  function link(scope, el, atr) {
    const modelValue = el.attributes['ng-model'].value;
    el.oninput = e => {
      scope[modelValue] = e.target.value;
      scope.$apply('ngModel');
    };
  }
  return {
    link
  };
}
export function ngBind() {
  function link(scope, el, atr) {
    const bindValue = el.attributes['ng-bind'].value;
    function bindNewValue() {
      el.innerText = scope[bindValue];
    }
    bindNewValue();
    scope.$watch('ngBind', bindNewValue);
  }
  return {
    link
  };
}
export function ngUppercase() {
  function link(scope, el, atr) {
    function transformTextUpperCase() {
      el.innerText = el.innerText.toUpperCase();
    }
    transformTextUpperCase();
    scope.$watch('ngUppercase', transformTextUpperCase);
  }
  return {
    link
  };
}
export function ngShow() {
  function link(scope, el, atr) {
    function showElement() {
      const elValue = Boolean(eval(el.attributes['ng-show'].value));

      if (elValue === true) {
        el.style.display = 'block';
        return;
      }
      el.style.display = 'none';
    }
    showElement();
    scope.$watch('ngShow', showElement);
  }
  return {
    link
  };
}
export function ngHide() {
  function link(scope, el, atr) {
    function showElement() {
      const elValue = Boolean(eval(el.attributes['ng-hide'].value));

      if (elValue === false) {
        el.style.display = 'block';
        return;
      }
      el.style.display = 'none';
    }
    showElement();
    scope.$watch('ngHide', showElement);
  }
  return {
    link
  };
}
export function ngMakeShort() {
  function link(scope, el, atr) {
    const atrValue = el.attributes['ng-make-short'].value;
    function cutTheText() {
      if (el.innerText.length <= atrValue) {
        return;
      }
      el.innerText = `${el.innerText.slice(0, atrValue)}...`;
    }
    cutTheText();
    scope.$watch('ngMakeShort', cutTheText);
  }
  return {
    link
  };
}
export function ngClick() {
  function link(scope, el, atr) {
    el.onclick = () => eval(el.attributes['ng-click'].value);
  }
  return {
    link
  };
}
export function ngIncludes() {
  function link(scope, el, atr) {
    function replaceText() {
      const matchVal = el.attributes['ng-includes'].value;
      const allText = el.innerText;

      if (allText.includes(matchVal)) {
        el.innerHTML = allText.replace(new RegExp(matchVal, 'g'), matchVal.toUpperCase());
      }
    }
    replaceText();
    scope.$watch('ngIncludes', replaceText);
  }
  return {
    link
  };
}